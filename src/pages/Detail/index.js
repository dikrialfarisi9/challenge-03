import {
  StyleSheet, StatusBar, FlatList, View, Image, Text, RefreshControl,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import Banner from '../../components/DetailScreen/Banner';
import Header from '../../components/DetailScreen/Header';
import Content from '../../components/DetailScreen/Content';

import Loading from '../../assets/loading';
import Color from '../../utils/color';
import { DetailMovies } from '../../api';

function Detail({ route }) {
  const [screenStatus, setScreenStatus] = useState(false);
  const [data, getData] = useState({});
  const [error, setError] = useState(false);
  const [message, setMessage] = useState('');
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    DetailMovies.getDetailMovies(getData, setScreenStatus, route.params.movieId, setError, setMessage);
  }, [route.params.movieId]);

  function DetailScreenStatusBar() {
    const focus = useIsFocused();

    return focus ? (
      <StatusBar backgroundColor={Color.BACKGROUND_COLOR} />
    ) : null;
  }

  function SimpleAlert() {
    return (
      <View>
        {alert(message)}
      </View>
    );
  }

  const onRefresh = () => {
    setRefresh(true);
    setScreenStatus(false);
    DetailMovies.getDetailMovies(getData, setScreenStatus, route.params.movieId, setError, setMessage);
    setRefresh(false);
  };

  if (screenStatus) {
    if (error) {
      return (
        <SimpleAlert />
      );
    }

    return (
      <InternetConnectionAlert
        onChange={(connectionState) => {
          console.log('Connection State: ', connectionState);
        }}
      >
        <SafeAreaView style={styles.container}>
          <FlatList
            refreshControl={
              <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
            }
            numColumns={3}
            columnWrapperStyle={{ marginHorizontal: 20 }}
            data={data.credits.cast}
            // keyExtractor={(item, index) => index}
            ListHeaderComponent={() => (
              <>
                <DetailScreenStatusBar />
                <Header movieData={data} />
                <Banner movieData={data} />
                <Content movieData={data} />
                <View style={styles.cast}>
                  <Text style={styles.txtCast}>Actors/Artist</Text>
                </View>
              </>
            )}
            renderItem={({ item }) => (
              <View style={styles.containerCast}>
                <Image style={styles.imgCast} source={{ uri: item.profile_path }} />
                <View style={styles.boxNameCast}>
                  <Text style={styles.titleCast}>{item.name}</Text>
                </View>
              </View>
            )}
          />
        </SafeAreaView>
      </InternetConnectionAlert>
    );
  }
  if (error) {
    return (
      <SimpleAlert />
    );
  }

  return (
    <Loading />
  );
}

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUND_COLOR,
  },
  cast: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  txtCast: {
    fontSize: 20,
    color: Color.TEXT_COLOR,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  imgCast: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  titleCast: {
    color: Color.TEXT_COLOR,
    fontSize: 12,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  containerCast: {
    alignItems: 'center',
    borderRadius: 10,
    marginBottom: 8,
    paddingRight: 12,
  },
  boxNameCast: {
    borderRadius: 4,
    padding: 2,
    width: 90,
    marginTop: 5,
  },
});
