import { StatusBar } from 'react-native';
import React from 'react';
import { useIsFocused } from '@react-navigation/native';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import LottieView from 'lottie-react-native';
import { IconLoading } from '../index';
import Color from '../../utils/color';

function Loading() {
  function StatusBarScreen() {
    const focus = useIsFocused();

    return focus ? (
      <StatusBar backgroundColor={Color.BACKGROUND_COLOR} />
    ) : null;
  }

  return (
    <InternetConnectionAlert
      onChange={(connectionState) => {
        console.log('Connection State: ', connectionState);
      }}
    >
      <StatusBarScreen />
      <LottieView
        source={IconLoading}
        autoPlay
        loop
        style={{ backgroundColor: Color.BACKGROUND_COLOR }}
      />
    </InternetConnectionAlert>
  );
}

export default Loading;
