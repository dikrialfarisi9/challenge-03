// Loading
import IconLoading from './icon/loading.json';

// Icon
import Search from './icon/search.png';
import Back from './icon/back.png';
import Love from './icon/love.png';
import Share from './icon/share.png';

// Image
import Foto from './image/dzikri.png';

export {
  IconLoading,
  Search,
  Foto,
  Back,
  Love,
  Share,
};
