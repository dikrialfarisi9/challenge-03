export function Date(date) {
  const splitDate = date.split('-');
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  let tempMonth = '';
  const splitMonth = splitDate[1].split('');

  if (splitMonth[0] === '0') {
    tempMonth = splitMonth[1];
  } else {
    tempMonth = splitMonth.join('');
  }

  const day = splitDate[2];
  const month = months[Number(tempMonth - 1)];
  const year = splitDate[0];

  return `${day} ${month} ${year}`;
}
