import * as Movies from './getMovies';
import * as DetailMovies from './getDetailMovies';

export { Movies, DetailMovies };
