import axios from 'axios';

export const getMovies = async (setData, setScreenStatus, error, message) => {
  await axios
    .get('http://code.aldipee.com/api/v1/movies')
    .then((req) => {
      setData(req.data);
      setScreenStatus(true);
    })
    .catch((err) => {
      console.log(err);
      error(true);
      message(err);
    });
};
