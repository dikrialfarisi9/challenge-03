import axios from 'axios';

export const getDetailMovies = async (setData, setScreenStatus, id, error, message) => {
  await axios
    .get(`http://code.aldipee.com/api/v1/movies/${id}`)
    .then((req) => {
      setData(req.data);
      setScreenStatus(true);
    })
    .catch((err) => {
      console.log(err);
      error(true);
      message(err);
    });
};
