import {
  StyleSheet, Text, View, Image, TouchableOpacity,
} from 'react-native';
import React from 'react';

import Color from '../../utils/color';
import { Search, Foto } from '../../assets';

function Header() {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.headerTitle}>Hi, Dzikri</Text>
      <View style={styles.boxHeader}>
        <TouchableOpacity>
          <Image source={Search} style={styles.search} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={Foto} style={styles.foto} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Header;
const styles = StyleSheet.create({
  headerContainer: {
    marginVertical: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerTitle: {
    flex: 3,
    fontSize: 18,
    color: Color.TEXT_COLOR,
    fontWeight: 'bold',
  },
  boxHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  search: {
    width: 20,
    height: 20,
  },
  foto: {
    height: 25,
    width: 25,
    borderRadius: 50,
  },
});
