import {
  StyleSheet, View, Image, TouchableOpacity, ImageBackground,
} from 'react-native';
import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import ShareSocial from 'react-native-share';
import { Share, Love, Back } from '../../assets';

import ImgToBase64 from 'react-native-image-base64';

import * as Date from '../../models/Date';

function Header(props) {
  const navigation = useNavigation();
  const [image, setImage] = useState('');

  const myShare = async () => {

    ImgToBase64.getBase64String(`${props.movieData.backdrop_path}`)
      .then(base64String => setImage(base64String))
      .catch(err => console.log(err));

    console.log(image);
    let foto = `data:image/jpeg;base64,${image}`

    const shareOptions = {
      message: `Haii.. Jangan Lupa Saksikan \n\nFilm: *${props.movieData.title}*\nTanggal: *${Date.Date(props.movieData.release_date)}*\nDibioskop Kesayangan Kalian!!\n\nPoster dapat dilihat pada link dibawah ini:\n${props.movieData.backdrop_path}\n\nJangan Sampai Ketinggalan!!`,
      url: foto,
    };

    try {
      const ShareResponse = await ShareSocial.open(shareOptions);
    } catch (err) {
      console.log('Error => ', err);
    }
  };

  return (
    <ImageBackground
      source={{ uri: props.movieData.backdrop_path }}
      style={[styles.poster]}
    >
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image source={Back} style={styles.headerIcon} />
        </TouchableOpacity>

        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity>
            <Image source={Love} style={[styles.headerIcon, styles.love]} />
          </TouchableOpacity>

          <TouchableOpacity onPress={myShare}>
            <Image source={Share} style={styles.headerIcon} />
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
}

export default Header;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 12,
    marginHorizontal: 20,
  },
  poster: {
    height: 180,
  },
  headerIcon: {
    height: 30,
    width: 30,
  },
  love: {
    marginRight: 20,
  },
});
