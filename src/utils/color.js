const Color = {
  BACKGROUND_COLOR: '#282142',
  CONTAINER_COLOR: '#322F58',
  TEXT_COLOR: '#FFFFFF',
  TEXT_COLOR_2: '#B5B1E5',
};

export default Color;
