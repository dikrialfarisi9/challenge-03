import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

import Splash from './pages/Splash';

function App() {
  return <Splash />;
}

export default App;

const styles = StyleSheet.create({});
