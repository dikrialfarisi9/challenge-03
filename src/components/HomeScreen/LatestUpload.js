import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

import { AirbnbRating } from 'react-native-ratings';

import React from 'react';
import { useNavigation } from '@react-navigation/native';

import Genre from '../../models/DataGenre';
import Color from '../../utils/color';
import * as Dates from '../../models/Date';

function LatestUpload(props) {
  const navigation = useNavigation();

  const Genres = (props) => {
    const { keys } = props; // genre_ids
    const { list } = props; // array genre

    return keys.map((key, index) => {
      const genre = Object.keys(list).find((data) => list[data] === key);

      return (
        <View style={styles.genre} key={index}>
          <Text style={styles.genreText}>{genre}</Text>
        </View>
      );
    });
  };

  function sortData(props) {
    const data = props;

    const sort = data.slice(0);
    sort.reverse((a, b) => (
      new Date(a.release_date).getTime() - new Date(b.release_date).getTime()
    ));

    return sort;
  }

  return (
    <View style={styles.latestContainer}>
      <Text style={styles.latestTitle}>Latest Upload</Text>
      <View style={styles.listParent}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={sortData(props.data)}
          scrollEnabled={false}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={[styles.listMovie, styles.shadowProp]}>
              <View style={styles.listImageMovie}>
                <Image
                  source={{ uri: item.poster_path }}
                  style={styles.imagePoster}
                />
              </View>
              <View style={styles.listContent}>
                <View style={styles.listDataContent}>
                  <Text style={styles.listTitle}>{item.title}</Text>
                  <Text style={styles.listlDate}>
                    {Dates.Date(item.release_date)}
                  </Text>
                  <View style={styles.ratingContainer}>
                    <AirbnbRating
                      count={5}
                      showRating={false}
                      isDisabled
                      defaultRating={item.vote_average / 2}
                      size={13}
                    />
                    <Text style={styles.ratingText}>
                      /
                      {item.vote_average}
                    </Text>
                  </View>
                  <View style={styles.listGenres}>
                    <Genres keys={item.genre_ids} list={Genre} />
                  </View>
                </View>
                <View style={styles.showButtonContainer}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Detail', { movieId: item.id })}
                    style={styles.showButton}
                  >
                    <Text style={styles.showButtonText}>Show More</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        />
      </View>
    </View>
  );
}

export default LatestUpload;

const styles = StyleSheet.create({
  latestContainer: {
    flex: 1,
  },
  shadowProp: {
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 3,
  },
  latestTitle: {
    color: Color.TEXT_COLOR,
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  imagePoster: {
    height: 160,
    width: 95,
    borderRadius: 8,
  },
  listMovie: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 20,
    backgroundColor: Color.CONTAINER_COLOR,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  listImageMovie: {
    flex: 1,
  },
  listContent: {
    flex: 2,
    marginLeft: 6,
  },
  listTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: Color.TEXT_COLOR,
  },
  listlDate: {
    marginTop: 5,
    color: Color.TEXT_COLOR,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingIcon: {
    width: 15,
    height: 15,
  },
  ratingText: {
    marginLeft: 5,
    color: Color.TEXT_COLOR,
  },
  listGenres: {
    width: 190,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  listDataContent: {
    height: 100,
  },
  genreText: {
    color: Color.TEXT_COLOR,
    fontSize: 11,
  },
  genre: {
    backgroundColor: Color.BACKGROUND_COLOR,
    paddingVertical: 2,
    paddingHorizontal: 5,
    borderRadius: 5,
    marginRight: 3,
    marginVertical: 2,
  },
  showButtonContainer: {
    flex: 1,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 35,
    backgroundColor: Color.BACKGROUND_COLOR,
    borderRadius: 10,
    marginTop: 25,
  },
  showButtonText: {
    color: Color.TEXT_COLOR,
    fontSize: 13,
    fontWeight: 'bold',
  },
});
