import {
  StyleSheet,
  StatusBar,
  ScrollView,
  LogBox,
  View,
  RefreshControl,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import { Movies } from '../../api/index';

import Color from '../../utils/color';

import Recommended from '../../components/HomeScreen/Recommended';
import Header from '../../components/HomeScreen/Header';
import LatestUpload from '../../components/HomeScreen/LatestUpload';
import Loading from '../../assets/loading';

function Home() {
  const [screenStatus, setScreenStatus] = useState(false);
  const [data, getData] = useState({});
  const [error, setError] = useState(false);
  const [message, setMessage] = useState('');
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    Movies.getMovies(getData, setScreenStatus, setError, setMessage);
  }, []);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  // alertErrorAPI
  function SimpleAlert() {
    return <View>{alert(message)}</View>;
  }

  // statusBar
  function HomeScreenStatusBar() {
    const focus = useIsFocused();

    return focus ? (
      <StatusBar backgroundColor={Color.BACKGROUND_COLOR} />
    ) : null;
  }

  // refresh
  const onRefresh = () => {
    setRefresh(true);
    setScreenStatus(false);
    Movies.getMovies(getData, setScreenStatus, setError, setMessage);
    setRefresh(false);
  };

  // component
  if (screenStatus) {
    if (error) {
      return <SimpleAlert />;
    }

    return (
      <InternetConnectionAlert
        onChange={(connectionState) => {
          console.log('Connection State: ', connectionState);
        }}
      >
        <SafeAreaView style={styles.container}>
          <HomeScreenStatusBar />
          <Header />
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
            }
          >
            <Recommended data={data.results} />
            <LatestUpload data={data.results} />
          </ScrollView>
        </SafeAreaView>
      </InternetConnectionAlert>
    );
  }
  if (error) {
    return <SimpleAlert />;
  }
  return <Loading />;
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.BACKGROUND_COLOR,
    paddingHorizontal: 20,
  },
});
