import {
  StyleSheet, Text, View, FlatList,
} from 'react-native';
import React from 'react';

import Color from '../../utils/color';

function Content(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.genres}>Genres</Text>
      <View style={{ marginBottom: 20 }}>
        <FlatList
          scrollEnabled={false}
          numColumns={3}
          data={props.movieData.genres}
          keyExtractor={(item, index) => index}
          renderItem={({ item }) => (
            <View style={styles.listGenres}>
              <Text style={styles.genreItem}>{item.name}</Text>
            </View>
          )}
        />
      </View>
      <Text style={styles.genres}>Synopshis</Text>
      <Text style={styles.synopshis}>{props.movieData.overview}</Text>
    </View>
  );
}

export default Content;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
  },
  genres: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Color.TEXT_COLOR,
  },
  listGenres: {
    backgroundColor: Color.CONTAINER_COLOR,
    marginRight: 6,
    borderRadius: 10,
    marginTop: 12,
  },
  genreItem: {
    paddingHorizontal: 12,
    paddingVertical: 7,
    color: Color.TEXT_COLOR,
  },
  synopshis: {
    fontSize: 15,
    color: Color.TEXT_COLOR_2,
    paddingTop: 12,
    textAlign: 'justify',
  },
});
