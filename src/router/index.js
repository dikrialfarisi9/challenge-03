import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../pages/Home';
import Detail from '../pages/Detail';

const Stack = createNativeStackNavigator();
const router = () => (
  <Stack.Navigator initialRouteName="Home">
    <Stack.Screen
      name="Home"
      component={Home}
      options={{ headerShown: false }}
    />

    <Stack.Screen
      name="Detail"
      component={Detail}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
);

export default router;
