import {
  StyleSheet, Text, View, FlatList, TouchableOpacity, Image,
} from 'react-native';
import React from 'react';

import { useNavigation } from '@react-navigation/native';
import Color from '../../utils/color';

function Recommended(props) {
  const navigation = useNavigation();
  return (
    <View>
      <Text style={styles.judulPoster}>Recommended</Text>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={props.data}
        keyExtractor={(item) => item.id}
        horizontal
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.posterContainer}
            onPress={() => navigation.navigate('Detail', { movieId: item.id })}
          >
            <Image
              style={styles.posterImage}
              source={{ uri: item.poster_path }}
            />
            <Text style={styles.posterText}>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

export default Recommended;

const styles = StyleSheet.create({
  posterContainer: {
    flex: 1,
  },
  judulPoster: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Color.TEXT_COLOR,
    marginBottom: 18,
  },
  posterText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: Color.TEXT_COLOR,
    width: 150,
  },
  posterImage: {
    marginRight: 20,
    width: 150,
    height: 220,
    borderRadius: 8,
    marginBottom: 10,
  },
});
