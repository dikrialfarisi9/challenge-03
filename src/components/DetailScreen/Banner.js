import {
  StyleSheet, Text, View, FlatList, Image,
} from 'react-native';
import React from 'react';

import { AirbnbRating } from 'react-native-ratings';
import Color from '../../utils/color';
import * as Date from '../../models/Date';

function Banner(props) {
  function convertRuntime(time) {
    const minute = time % 60;
    const hour = Math.floor(time / 60);

    return `${hour}hr ${minute} min`;
  }

  return (
    <View style={styles.latestContainer}>
      <View style={styles.listParent}>
        <View style={[styles.listMovie, styles.shadowProp]}>
          <View style={styles.listImageMovie}>
            <Image
              source={{ uri: props.movieData.poster_path }}
              style={styles.imagePoster}
            />
          </View>
          <View style={styles.listContent}>
            <View>
              <View style={styles.listDataContent}>
                <Text style={styles.listTitle}>{props.movieData.title}</Text>
                <Text style={styles.listlDate}>
                  {Date.Date(props.movieData.release_date)}
                </Text>
                <View style={styles.bannerStatus}>
                  <View>
                    <Text style={styles.statusRuntime}>
                      {props.movieData.status}
                    </Text>
                  </View>
                  <View>
                    <Text style={styles.statusRuntime}>
                      {convertRuntime(props.movieData.runtime)}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.ratingContainer}>
                <AirbnbRating
                  count={5}
                  showRating={false}
                  isDisabled
                  defaultRating={props.movieData.vote_average / 2}
                  size={16}
                />
                <Text style={styles.ratingText}>
                  /
                  {' '}
                  {props.movieData.vote_average}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

export default Banner;

const styles = StyleSheet.create({
  latestContainer: {
    flex: 1,
    marginTop: -80,
    marginHorizontal: 20,
  },
  shadowProp: {
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 5,
  },
  imagePoster: {
    height: 140,
    width: 95,
    borderRadius: 8,
  },
  listMovie: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 20,
    backgroundColor: Color.CONTAINER_COLOR,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  listImageMovie: {
    flex: 1,
  },
  listContent: {
    flex: 2,
    marginLeft: 6,
    justifyContent: "space-around"
  },
  listTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Color.TEXT_COLOR,
  },
  listlDate: {
    marginTop: 5,
    color: Color.TEXT_COLOR,
    fontSize: 13,
  },
  bannerStatus: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
  },
  statusRuntime: {
    backgroundColor: Color.BACKGROUND_COLOR,
    paddingHorizontal: 13,
    paddingVertical: 5,
    borderRadius: 5,
    color: Color.TEXT_COLOR,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 110,
    height: 20
  },
  ratingIcon: {
    width: 15,
    height: 15,
  },
  ratingText: {
    marginLeft: 5,
    color: Color.TEXT_COLOR,
    fontSize: 16,
  },
  listDataContent: {
    height: 100,
  },
  showButtonContainer: {
    flex: 1,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 35,
    backgroundColor: Color.BACKGROUND_COLOR,
    borderRadius: 10,
    marginTop: 25,
  },
  showButtonText: {
    color: Color.TEXT_COLOR,
    fontSize: 13,
    fontWeight: 'bold',
  },
});
